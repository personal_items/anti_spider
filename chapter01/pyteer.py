"""pyteer测试"""
import asyncio
from pyppeteer import launch


async def main():
    """
    测试打开页面并截图
    """
    # 1.初始化浏览器对象
    browser = await launch()
    page = await browser.newPage()
    # 2.访问指定URL
    await page.goto('https://www.baidu.com/')
    # 3.打开网址后进行截图并保存在当前路径
    await page.screenshot({ 'path': 'baidu.png' })
    # 4.关闭浏览器对象
    await browser.close()


asyncio.get_event_loop().run_until_complete(main())