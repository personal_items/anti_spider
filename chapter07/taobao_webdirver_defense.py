"""
淘宝网WebDriver识别
"""

import os
import time
from selenium import webdriver

DIR_NAME = os.path.abspath(os.path.join(os.getcwd(), '..'))
DRIVER_EXE = os.path.join(DIR_NAME, 'utils', 'chromedriver')

url = 'https://login.taobao.com/member/login.jhtml'
driver = webdriver.Chrome(executable_path=DRIVER_EXE)
driver.get(url)
# todo JavaScript脚本
script = "Object.defineProperty(navigator, 'webdriver', {get: () => false})"
# todo 执行JavaScript脚本代码
driver.execute_script(script)
driver.maximize_window()
# todo 输入测试账号及密码
driver.find_element_by_css_selector('#fm-login-id').send_keys('13011112222')
time.sleep(1)
driver.find_element_by_css_selector('#fm-login-password').send_keys('88888888')
time.sleep(1)
driver.find_element_by_css_selector('#login-form .password-login').submit()
time.sleep(1)
# todo 保存截图
driver.save_screenshot('./taobao.png')