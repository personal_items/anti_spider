"""
计算型验证码
使用程序通过登录页面中的计算型验证码校验
目标站点：http://www.porters.vip/captcha/mathes.html
"""
try:
    from PIL import Image
except (ImportError, ) as e:
    import Image
import pytesseract
import os

image = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'matches_captcha.png')
# todo 使用pytesseract进行验证码识别
text = pytesseract.image_to_string(image)
print(text)