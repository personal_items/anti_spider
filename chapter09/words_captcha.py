"""
字符验证码
使用程序通过目标站点登录界面中的字符验证码校验
目标站点：http://www.porters.vip/captcha/words.html
"""
import os
import pytesseract

try:
    from PIL import Image
except (ImportError,) as e:
    import Image


# todo 图片二值化处理
def handler(grays, threshold=160):
    """图片二值化处理"""
    table = []
    for i in range(256):
        if i < threshold:
            table.append(0)
        else:
            table.append(1)
    anti = grays.point(table, '1')
    return anti


# todo 本地图片验证码
images = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'words_captcha.png')

"""无法直接使用pytesseract识别验证码"""
# # todo 使用pytesseract识别验证码中的字符并打印
# captcha_text = pytesseract.image_to_string(images)
# print('使用pytesseract识别验证码中的字符并打印...')
# print(captcha_text)

# todo 1.图片灰度处理
"""灰度后，字符颜色和背景色反差并不明显，需进行二值化处理"""
gray = Image.open(images).convert('L')
# gray.show()
# todo 2.图片二值化处理
image = handler(gray)
# image.show()
# todo 3.使用pytesseract识别验证码中的字符并打印
captcha_text = pytesseract.image_to_string(image)
print('使用pytesseract识别验证码中的字符并打印...')
print(captcha_text)