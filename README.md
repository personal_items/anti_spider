# anti_spider

#### 介绍
python 反爬虫原理及绕过实战

#### 软件架构
[在线练习平台](http://www.porters.vip/)

[腾讯云OCR](https://cloud.tencent.com/product/ocr-catalog)

[浏览器指纹API](https://fingerprintjs.com/)


#### 安装教程
1. 导出项目依赖
> pip freeze > requirements.txt

2. 安装项目依赖
> pip install -r requirements.txt

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 目录说明
* chapter01 开发环境配置
    * pyteer.py - pyteer测试
* chapter02 Web网站的构成和页面渲染
    * wsocket.py - WebSocket连接
* chapter04 信息校验型反爬虫
    * user_agent.py - User-Agent反爬虫
    * cookie_spider.py - Cookie反爬虫
    * sign.py - 签名验证反爬虫
    * youdao_spider.py - 有道翻译反爬虫
* chapter05 动态渲染反爬虫
    * pyppeteer_demo.py - 异步渲染库Puppeteer
* chapter06 文本混淆反爬虫
    * img_confusion.py - 图片伪装反爬虫
    * css_translate.py - CSS偏移反爬虫
    * qunar_css_translate.py - 去哪儿网反爬虫(CSS偏移反爬虫)
    * svg_camouflage.py - SVG映射反爬虫
    * font_camouflage.py - 字体反爬虫
* chapter07 特征识别反爬虫
    * webdirver_defense.py - WebDriver识别
    * taobao_webdirver_defense.py - 淘宝网WebDriver识别
    * browser_features_defense.py - 浏览器特征识别
    * access_rate_features_defense.py - 访问频率限制
    * hide_link_defense.py - 隐藏链接反爬虫
* chapter09 验证码
    * words_captcha.py - 字符验证码
    * matches_captcha.py - 计算型验证码
    * sliders_captcha.py - 滑动验证码
    * jigsaw_captcha.py - 滑动拼图验证码
    * jigsaw_canvas_captcha.py - 滑动拼图验证码(Canvas版本)
* chapter10 综合知识
    * rsa_encrypt.py - 非对称加密与RSA