"""
签名验证反爬虫
爬取目标网站（http://www.porters.vip/verify/sign/）公告页面中的公告详情
"""
import re
import random
import time
import hashlib
import requests
from lxml import etree


def generate_md5(value):
    """生成md5签名"""
    md5 = hashlib.md5()
    md5.update(value.encode('utf-8'))
    return md5.hexdigest()


action = ''.join([str(num) for num in random.sample(range(0, 10), 5)])  # todo 随机5位数字
tim = str(int(time.time()))     # todo 时间戳秒数
rand_str = ''.join([chr(random.randint(65, 90)) for _ in range(5)])     # todo 大写英文字母
hex_s = generate_md5(action + tim + rand_str)

url = 'http://www.porters.vip/verify/sign/fet'
params = {
    'actions': action,
    'tim': tim,
    'randstr': rand_str,
    'sign': hex_s
}
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
    'X-Requested-With': 'XMLHttpRequest'
}
res = requests.get(url, params=params, headers=headers)
if res.status_code == 200:
    parser = etree.HTMLParser(encoding='utf-8')
    html = etree.HTML(res.text, parser=parser)
    items = html.xpath('//p//text()')
    for item in items:
        text = re.sub(re.compile('[\s]', re.DOTALL), '', item)
        print(text)
