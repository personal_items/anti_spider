"""
WebDriver识别
"""

import os
import time
from selenium import webdriver

DIR_NAME = os.path.abspath(os.path.join(os.getcwd(), '..'))
DRIVER_EXE = os.path.join(DIR_NAME, 'utils', 'chromedriver')


url = 'http://www.porters.vip/features/webdriver.html'
driver = webdriver.Chrome(executable_path=DRIVER_EXE)
driver.get(url)
driver.maximize_window()
# todo JavaScript脚本
script = "Object.defineProperty(navigator, 'webdriver', {get: () => false})"
# todo 执行JavaScript脚本代码
driver.execute_script(script)
driver.find_element_by_css_selector('#article button').click()
time.sleep(3)
content = driver.find_element_by_css_selector('#content').text
print(content)
driver.save_screenshot('./webdriver.png')