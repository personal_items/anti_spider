"""
Cookie反爬虫
爬取目标站点(http://www.porters.vip/verify/cookie/content.html)公告详情中的公告标题
"""
import requests
from lxml import etree

url = 'http://www.porters.vip/verify/cookie/content.html'
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
    'Cookie': 'isfirst=789kq7uc1pp4c'
}
res = requests.get(url, headers=headers)
if res.status_code == 200:
    parser = etree.HTMLParser(encoding='utf-8')
    html = etree.HTML(res.text, parser=parser)
    header = html.xpath('//*[contains(@class, "page-header")]//h1/text()')[0]
    print(header)