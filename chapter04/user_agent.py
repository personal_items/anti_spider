"""
User-Agent反爬虫
爬取目标网站(http://www.porters.vip/verify/uas/index.html)右侧“本周热点”列表中的新闻标题
"""
import re
import requests

url = 'http://www.porters.vip/verify/uas/index.html'
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
}
res = requests.get(url, headers=headers)
if res.status_code == 200:
    html = res.text
    list_group = re.finditer(re.compile('<.*?class="list-group-item.*?".*?>(.*?)<.*?>'), html)
    for item in list_group:
        print(item.group(1))