"""
异步渲染库Puppeteer
使用pypeter爬虫目标站点：http://www.porters.vip/verify/sign/，详情数据
"""

import asyncio
from pyppeteer import launch


async def main():
    """获取目标网站详情数据"""
    # 1.初始化浏览器对象
    brower = await launch()
    # 2.创建新页面
    page = await brower.newPage()
    # 3.打开目标网站
    await page.goto('http://www.porters.vip/verify/sign/')
    # 4.点击查看详情按钮
    await page.click('#fetch_button')
    # 5.读取页面指定位置的文本
    resp = await page.xpath('//*[@id="content"]')
    text = await (await resp[0].getProperty('textContent')).jsonValue()
    print(text)
    # 6.关闭浏览器对象
    await brower.close()


asyncio.get_event_loop().run_until_complete(main())
