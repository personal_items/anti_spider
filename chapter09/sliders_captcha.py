"""
滑动验证码
使用程序通过登录界面中滑动验证码的校验
目标站点：http://www.porters.vip/captcha/sliders.html
"""
import time
import os
from selenium import webdriver

DIR_NAME = os.path.abspath(os.path.join(os.getcwd(), '..'))
DRIVER_EXE = os.path.join(DIR_NAME, 'utils', 'chromedriver')

# todo 1.打开页面并定位滑块元素
brower = webdriver.Chrome(executable_path=DRIVER_EXE)
brower.get('http://www.porters.vip/captcha/sliders.html')
brower.maximize_window()
# 定位滑块
hover = brower.find_element_by_css_selector('.hover')

# todo 2.按下并移动鼠标
action = webdriver.ActionChains(brower)
action.click_and_hold(hover).perform()  # todo 点击并保持不松开
action.move_by_offset(340, 0)   # todo 设置滑动距离，横向距离340px，纵向距离0px
action.release().perform()  # todo 松开鼠标

time.sleep(3)
brower.close()