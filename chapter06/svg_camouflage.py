"""
SVG映射反爬虫
爬取目标站点商家联系电话、店铺地址、评分数据
http://www.porters.vip/confusion/food.html
"""
import re
import requests
from lxml import etree

url = 'http://www.porters.vip/confusion/food.html'
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                  'Chrome/92.0.4515.159 Safari/537.36 '
}
mapping = {
    'vhkbvu': 4,
    'vhk08k': 0,
    'vhk84t': 5,
    'vhk6zl': 1,
    'vhkqsc': 7
}

res = requests.get(url, headers=headers)
parser = etree.HTMLParser(encoding='utf-8')
html = etree.HTML(res.text, parser=parser)
shop_name = html.xpath('//*[contains(@class, "title")]/text()')[0].strip()  # todo 店铺名称
address = html.xpath('//*[contains(@class, "address")]//*[contains(@class, "address_detail")]//text()')[
    0].strip()  # todo 店铺地址
phone_ele = html.xpath('//*[contains(@class, "more")]//d/@class')
telephone = ''.join([str(mapping.get(item, '-')) for item in phone_ele])  # todo 电话
print(shop_name, address, telephone)
