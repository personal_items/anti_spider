"""
浏览器特征识别
"""

import os
import time
from selenium import webdriver

DIR_NAME = os.path.abspath(os.path.join(os.getcwd(), '..'))
DRIVER_EXE = os.path.join(DIR_NAME, 'utils', 'chromedriver')

url = 'http://www.porters.vip/features/browser.html'
driver = webdriver.Chrome(executable_path=DRIVER_EXE)
driver.get(url)
time.sleep(1)
driver.maximize_window()
time.sleep(1)
driver.save_screenshot('./browser_features.png')